package com.acn;

import java.io.IOException;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class GAE_Export_CSVServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
		 StringBuffer buffer = new StringBuffer();
		  buffer.append("header1, header2, header3\n");
		  buffer.append("row1column1, row1column2, row1column3\n");
		  buffer.append("row2column1, row2column2, row2column3\n");
		  // Add more CSV data to the buffer

		  byte[] bytes = buffer.toString().getBytes();

		  // This will suggest a filename for the browser to use
		  resp.addHeader("Content-Disposition", "attachment; filename=\"myFile.csv\"");

		  resp.getOutputStream().write(bytes, 0, bytes.length);
	}
}
